<?php

$programmationDate = [
  '2018-10-10',
  '2018-10-12',
  '2018-10-13',
  '2018-10-14',
  '2018-10-18',
  '2018-10-20',
  '2018-10-22',
  '2018-10-23',
];

 

$date = (isset($_GET["date"])) ? $_GET["date"] : NULL;
$position = (isset($_GET["position"])) ? $_GET["position"] : NULL;


/**Les paramètres -1 -1 permettent d'initialiser le carousel**/ 
if ($date && $position) {
	if($date == -1 && $position == -1){
        $i = 0;
        $s = '';
        while( $i < 7){
            $s .= $programmationDate[$i] . ' ';
            $i++;
        }
        echo $s;
    }
    else if(is_string($date)){
        if($position == -1){
            echo $programmationDate[precedent($date)];
        }
        else if($position == 1){
            echo $programmationDate[suivant($date)];
        }
    }
}




/**retourne la date précédente à la date entrée en paramètre**/
function precedent($date){
	global $programmationDate;
	$i = 1;
	$taille = sizeof($programmationDate);
	if(strcmp($date,$programmationDate[0]) <= 0)
		return $taille-1;
	else{
		while($i < $taille){
			if(strcmp($date,$programmationDate[$i])==0){
				return $i -1;
			}
			else if(strcmp($date,$programmationDate[$i]) == -1){
				return $i;
			}
			else{
				$i++;
			}
		}
		
		return $i-1;
	}
}

/**retourne la date suivante entrée en paramètre**/
function suivant($date){
	global $programmationDate;
	$i = 1;
	$taille = sizeof($programmationDate);
	if(strcmp($date,$programmationDate[$taille -1]) >= 0)
		return 0;
	else{
		while($i < $taille - 1){
			if(strcmp($date,$programmationDate[$i])==0){
				return $i + 1;
			}
			else if(strcmp($date,$programmationDate[$i]) == -1){
				return $i;
			}
			else{
				$i++;
			}
		}
		
		return $i;
	}
}




?>