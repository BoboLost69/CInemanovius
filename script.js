document.addEventListener('DOMContentLoaded', function() {
    
    
    
    
    fg = document.getElementById('fleche_gauche');
    fd = document.getElementById('fleche_droite');
    car = document.getElementById('carousel_conteneur');

    carousel = new Carousel(car);
    request(DataInit, -1, -1);


    


    fg.addEventListener('click', function(){
        request(DataFlecheGauche, carousel.dateD(), 1);
        
    })
    
    fd.addEventListener('click', function(){
        request(DataFlecheDroite, carousel.dateG(), -1);
        
     })
    

});


class Carousel {
    
    constructor(element){
        this.element = element;
        this.translation = 0;
        this.tab = [];
        this.translation = 0;
    
    }
    
    translationGauche(){
        var i = 0;
        this.translation = this.translation - 134;
        for(i =0; i<this.tab.length; i++){
            this.tab[i].li.style.transform = "translateX(" +this.translation + "px)";
             
        }
        this.element.style.transform = "translateX(" + (-1)*this.translation + "px)";
        
            
    }
    
    translationDroite(){
        this.translation = this.translation + 134;
        var i = 0;
        for(i =0; i<this.tab.length; i++){
            this.tab[i].li.style.transform = "translateX(" +this.translation + "px)";
        }
        this.element.style.transform = "translateX(" + (-1)*this.translation + "px)";
    }
    
    ajoutElementGauche(text){
        var tmp = new ElementCarousel(text);
        this.element.insertBefore(tmp.li, this.element.firstChild);
        this.tab.unshift(tmp);
        
    }
    
    ajoutElementDroite(text){
        var tmp = new ElementCarousel(text);
        this.element.appendChild(tmp.li);
        this.tab.push(tmp);
        
    }
    
    supprimerElementDroite(){
        var last = this.element.lastChild;
        this.element.removeChild(last);
        this.tab.pop();
    }
    
    supprimerElementGauche(){
        var first = this.element.firstChild;
        this.element.removeChild(first);
        this.tab.shift();
    }
    
    dateD(){
        return this.tab[this.tab.length - 1].dateOriginal;
    }
    
    dateG(){
        return this.tab[0].dateOriginal;
    }

    init(text){  
        var tmp = text.split(' ');
        var i = 0;
        for(i=0; i<7; i++){
            this.tab[i] = new ElementCarousel(tmp[i]);
            this.element.appendChild(this.tab[i].li);
            
        }

    }
}


class ElementCarousel{

    constructor(text){
        this.date = dateToText(text);
        this.dateOriginal = text;
        var li = document.createElement('li');
        li.setAttribute('class', 'carousel');
        var tmp = document.createTextNode(this.date);
        li.appendChild(tmp);
        this.li = li;

    }
}

function dateToText(text){
    var d = new Date(text);
    var semaine = ['DIM','LU', 'MAR', 'MER', 'JEU', 'VEN', 'SAM'];
    var nd = '';
    nd += semaine[d.getDay()] + '. ';
    nd += d.getDate()  + '/';
    var mois = d.getMonth()+1;
    if(mois < 9)
        nd += '0' + (mois +1);
    else
        nd += mois;
    
    return nd;
}


function carouselElement(text){
    var li = document.createElement('li');
    li.setAttribute('class', 'carousel');
    var date = document.createTextNode(dateTotext(text));
    li.appendChild(date);
    var span = document.createElement('span');
    var date2 = document.createTextNode(text);
    console.log(date2);
    span.appendChild(date2);
    span.style.display = 'none';
    li.appendChild(span);
    return li;
}

/**requête asynchrone**/
function getXMLHttpRequest() {
	var xhr = null;
	if (window.XMLHttpRequest || window.ActiveXObject) {
		if (window.ActiveXObject) {
			try {
				xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		} else {
			xhr = new XMLHttpRequest(); 
		}
	} else {
		alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
		return null;
	}
	
	return xhr;
}





function request(callback, param1, param2) {
	var xhr = getXMLHttpRequest();
	
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			callback(xhr.responseText);
		}
	};
	
    var date = encodeURIComponent(param1);
    var position = encodeURIComponent(param2);
    xhr.open("GET", "programmation.php?date=" + date + "&position=" + position, true);
    xhr.send();
    }




function DataInit(Data){
    carousel.init(Data);
    request(DataInitD, carousel.dateD(), 1);
    request(DataInitG, carousel.dateG(), -1);
    
}

function DataInitD(Data){
    carousel.ajoutElementDroite(Data);
}

function DataInitG(Data){
    carousel.ajoutElementGauche(Data);
}

function DataFlecheDroite(Data){
    carousel.ajoutElementGauche(Data);
    carousel.supprimerElementDroite();
    carousel.translationDroite();

    
}

function DataFlecheGauche(Data){
    carousel.ajoutElementDroite(Data);
    carousel.supprimerElementGauche();
    carousel.translationGauche();
}







    
    
    

