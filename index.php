<!DOCTYPE html>
<html>

	<head>
        <meta charset="utf-8"  />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="style.css" />
		<script type="text/javascript" src="script.js" ></script>
		<title>Cinéma Novius</title>
	</head>
	<body>
		<div id="bloc_page">
            <header>
                <nav id="logo_cinema">
                    <div id="logo"><img src="assets/img/logo.png" /></div>
                    <div id="cinema">
                        <ul id="cinema_nav">
                            <li class="cinema"><a href="#">cinéma 1</a></li>
                            <li class="cinema"><a href="#">cinéma 2</a></li>
                            <li class="cinema"><a href="#">cinéma 3</a></li>
                         </ul>
                    </div>
                </nav>
                <nav id="nav_droit">
                    <nav>
                        <ul id="nav_sup">
                            <li id="pdf_nav">
                                <div id="download_info"><a href="#">programme pdf</a></div>
                                <div  id="pdf">
                                    <a href="#"><img src="assets/img/download.png" alt="Télécharger le programme"/></a>
                                </div>
                            </li>
                            <li id="infos_pratiques">
                              <ul>
                                  <li><a href="#">Nos Cinémas</a></li>
                                  <li><span class="union">-</span></li>
                                  <li><a href="#">Scolaires</a></li>
                                  <li><span class="union">-</span></li>
                                  <li><a href="#">Contact</a></li>
                                  <li><span class="union">-</span></li>
                                  <li><a href="#">Infos pratiques</a></li>
                                </ul>
                            </li>
                            <li id="social">
                            <ul>
                                <li>
                                    <div id="twitter">
                                        <a href="#"	><img src="assets/img/twitter.png" alt="Twitter"/></a>
                                    </div>
                                </li>
                                <li>
                                    <div id="facebook">
                                        <a href="#"	><img src="assets/img/facebook.png" alt="Facebook"/></a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li id="menu_billet">
                            <div id="bouton_billet"><a href="#">billeterie</a></div>
                            <div id="menu_contenu_billet">
                                <ul>
                                    <li><a href="#" class="cinema2">cinéma 1</a></li>
                                    <li><a href="#" class="cinema2">cinéma 2</a></li>
                                    <li><a href="#" class="cinema2">cinéma 3</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    
                </nav>
                <nav id="droit_inf">
                    <ul id="evenement_nav">
                        <li>
                            <div id="menu_programmation"> 
                                <div id="bouton_programmation" class="evenement"><a href="#">programmation</a></div>
								<div id="menu_contenu_programmation">
                                    <ul  id="liste_cinema3">
                                        <li><a href="#" class="cinema3" >Cinéma 1</a></li>
								        <li><a href="#" class="cinema3" >Cinéma 2</a></li>
								        <li><a href="#" class="cinema3" >Cinéma 3</a></li>
								        <li><a href="#" class="cinema3" >Calendrier Général</a></li>
                                    </ul>
								</div>
				            </div>
                        </li>
                        <li><span class="union2">-</span></li>
                        <li class="evenement"><a href="#">evenement</a></li>
                        <li><span class="union2">-</span></li>
                        <li class="evenement"><a href="#">avant-premières</a></li>
                        <li><span class="union2">-</span></li>
                        <li class="evenement"><a href="#">sortie nationale</a></li>
                        <li><span class="union2">-</span></li>
                        <li class="evenement"><a href="#">grand public</a></li>
                    </ul>
                    </nav>
                </nav>
            </header>
            <aside>
				<div id="bloc_aside">
					<div id="info_film">
						<div id="type_sortie">sortie nationale</div>
						<div id="titre_film">don't worry, </div>
						<div id="titre_film2">he won't get far on foot</div>
						<div id="auteur_film">de Gus Van Sant</div>
						<div id="annee_film">2018</div>
					</div>
					<div id="cercle_nav">
						<ul>
							<li><div class="cercle_noir"></div></li>
							<li><div class="cercle_rouge"></div></li>
							<li><div class="cercle_rouge"></div></li>
						</ul>
					</div>
				</div>
			</aside>
			
			<footer>

				<div id="bloc_bas">
					<div id="titre_programmation"><span id="prog">Programmatio</span><span id="n">n</span></div>
					<div id="bloc_date">
						<div id="fleche_gauche">
							<img id="fg0" src="assets/img/fleche_gauche0.png" alt="gauche" />
                            <img id="fg1" src="assets/img/fleche_gauche1.png" alt="gauche" />
						</div>
						<div id="programmation_date">
                            <div id="conteneur">
                               
							<ul id="carousel_conteneur"></ul>
                            </div>
						</div>
						<div id="fleche_droite">
							<img id="fd0" src="assets/img/fleche_droite0.png" alt="droite" />
                            <img id="fd1" src="assets/img/fleche_droite1.png" alt="droite" />
                            
						</div>
						<div id="bloc_cal">
							<div id="calendrier">
								<div id="calendrier_info"><a href="#">calendrier de la semaine</a></div>
								<div id="cal">
									<a href="#"><img src="assets/img/calendar.png" alt="calendrier"/></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>
        </div>
    </body>
</html>
